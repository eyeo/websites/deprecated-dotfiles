pip install Markdown Jinja2 lxml urllib3 werkzeug
cd $HOME
mkdir ".npm"
npm config set prefix "$HOME/.npm"
npm install -g gulp-cli eslint stylelint
mkdir ".gitlab"
cd ".gitlab"
git clone https://gitlab.com/eyeo/websites/cms.git
git clone https://gitlab.com/eyeo/websites/sitescripts.git
git clone https://gitlab.com/eyeo/websites/dotfiles.git
cd $HOME
echo ". $HOME/.gitlab/dotfiles/bashrc" >> ".bashrc"

